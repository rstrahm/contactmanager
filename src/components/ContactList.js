import React from "react";
import PropTypes from "prop-types";
import { Table } from "reactstrap";
import 'bootstrap/dist/css/bootstrap.min.css';

// import the Contact component
import Contact from "./Contact";

function ContactList(props) {
    return (
        <Table striped>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>E-mail</th>
                 </tr>
            </thead>
            <tbody>
                {props.contacts.map(c => <Contact key={c.id} id={c.id} name={c.name} email={c.email} />)}
            </tbody>
        </Table>
    );
}

ContactList.propTypes = {
  contacts: PropTypes.array.isRequired
};

export default ContactList;
