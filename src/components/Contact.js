import React from "react";
import PropTypes from "prop-types";

function Contact(props) {
  return (
        <tr>
            <th scope="row">
                <input
                    name="isGoing"
                    type="checkbox"
                    value={props.id}
                 />
                 {props.id}
            </th>
            <td>{props.name}</td>
            <td>{props.email}</td>
        </tr>
  );
}

Contact.propTypes = {
  name: PropTypes.string.isRequired
};

export default Contact;
